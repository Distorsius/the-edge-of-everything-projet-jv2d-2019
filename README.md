The edge of everything/le bord de tout

Projet de développement d'un jeu 2D ayant pour but de faciliter l'accès aux concepts les plus obscurs de la relativité.
Utilise Unity2d 2019.3 (Beta)

Pour jouer au jeu:
Téléchargez le dossier Build, puis ouvrez index.html dans le browser de votre choix.