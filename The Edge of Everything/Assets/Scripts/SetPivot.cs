﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPivot : MonoBehaviour
{
    public Transform transform;
    public GameObject target;

    void Update()
    {
        transform.position = new Vector2(target.transform.position.x,0.5f);
    }
}
