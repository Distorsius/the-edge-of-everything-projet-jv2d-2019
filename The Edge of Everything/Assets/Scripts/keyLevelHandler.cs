﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class keyLevelHandler : MonoBehaviour
{
    public int toLevel;
    public GameObject goal1;
    public GameObject goal2;
    public GameObject goal3;


    // Start is called before the first frame update
    void OnTriggerEnter2D(Collider2D other)
    {
        if(goal1.activeSelf == false && goal2.activeSelf == false && goal3.activeSelf == false){
            if(other.CompareTag("Player")){
                SceneManager.LoadScene(toLevel);
            }
        }
    }

}
