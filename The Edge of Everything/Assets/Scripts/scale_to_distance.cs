﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scale_to_distance : MonoBehaviour
{
    public GameObject target;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Figure out the current distance by finding the difference from starting distance
        float curDistance = Vector3.Distance(target.transform.position, transform.position);
        // or was it the other way around, this code is untested!

        //Scale this object depending on distance away to the starting distance
        transform.localScale = transform.localScale * (curDistance/100f);
    }


}
