﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextHandler : MonoBehaviour{

    public GameObject TextBox;
    public Text textstring;
    public TextAsset textfile;
    public string[] textlines;

    public int currentLine;
    public int endAtLine;

    public CharacterController2D player;
    // Start is called before the first frame update
    void Start(){

      player = FindObjectOfType<CharacterController2D>();
      if(textfile != null){
        textlines = (textfile.text.Split('\n'));
      }

      if(endAtLine == 0){
        endAtLine = textlines.Length -2;
      }
    }

    // Update is called once per frame
    void Update(){
      textstring.text = textlines[currentLine];
      if(Input.GetKeyDown(KeyCode.Return) && TextBox.activeSelf){
        currentLine += 1;
      }
      if(currentLine > endAtLine){
        TextBox.SetActive(false);
      }
    }
}
