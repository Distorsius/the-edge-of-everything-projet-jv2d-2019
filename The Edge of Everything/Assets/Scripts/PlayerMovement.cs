﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour {

        public CharacterController2D controller;
        public Animator animator;
        public Rigidbody2D self;

        public float runSpeed = 40f;

        float horizontalMove = 0f;
        bool jump = false;
        bool crouch = false;

        public GameObject TextBox;

         void Start(){
            TextBox = GameObject.FindWithTag("Text");
         }
         // Update is called once per frame
         void Update () {

            horizontalMove =  Input.GetAxisRaw("Horizontal") * runSpeed;

            animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

            if (Input.GetButtonDown("Jump"))
                  {
                       jump = true;
                       animator.SetBool("isJumping", true);
                  }
            if(self.velocity.y > 0.1f)
                  {
                        animator.SetBool("isJumping", true);
                  }
            if(self.velocity.y < -0.1f)
                  {
                        animator.SetBool("isFalling", true);
                  }
            if(self.velocity.y > -0.1 || self.velocity.y < 0.1)
                  {
                        animator.SetBool("isJumping", false);
                        animator.SetBool("isFalling", false);
                  }
          }
          
          public void OnLanding (){
                animator.SetBool("isJumping", false);
                animator.SetBool("isFalling", false);

          }
          void FixedUpdate ()
          {


                     Debug.Log(self.velocity.y);
                     controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
                     jump = false;
                   
          }
}
