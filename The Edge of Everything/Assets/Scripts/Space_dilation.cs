﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Space_dilation : MonoBehaviour
{
    public Transform transform;
    public Rigidbody2D player;
    public float TransformCoef = 10f;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
      Vector3 v3Velocity = Vector3.Scale(player.velocity,player.velocity) / 1000f;
      v3Velocity = v3Velocity*TransformCoef;

      Vector3 transformationVector = new Vector3(1,1,1);
      transformationVector = transformationVector - v3Velocity;

      if(transformationVector.x > 0.7f){
        transform.localScale = new Vector3(transformationVector.x,1,1);
      }
      else{
        transform.localScale = new Vector3(0.7f,1,1);
      }
     }

}
