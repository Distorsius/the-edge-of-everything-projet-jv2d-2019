﻿    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;
    using System.Threading;
    using UnityEngine.SceneManagement;

    public class timerHandler : MonoBehaviour {
        public int timeLeft = 60; //Seconds Overall
        public Text countdown; //UI Text Object
        public Rigidbody2D player;
        public int toLevel;

      void Start () {
        StartCoroutine("LoseTime");
        Time.timeScale = 1; //Just making sure that the timeScale is right
      }
      void Update () {
        countdown.text = ("" + timeLeft); //Showing the Score on the Canvas
        
      }
      //Simple Coroutine
      IEnumerator LoseTime(){
        while (timeLeft > 0){
            Debug.Log(1 + Mathf.Abs(player.velocity.x+player.velocity.y)/2);
            yield return new WaitForSeconds (1 + Mathf.Abs(player.velocity.x+player.velocity.y)/2);
            timeLeft--; 
        }
        SceneManager.LoadScene(toLevel);
      }
    }