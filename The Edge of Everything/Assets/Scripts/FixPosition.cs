﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixPosition : MonoBehaviour
{
    public Transform transform;
    private int facing = 1;

    void Update()
    {
        if(Input.GetAxisRaw("Horizontal") > 0){
            facing = 1;
        }
        if(Input.GetAxisRaw("Horizontal") < 0){
            facing = -1;
        }
        transform.localScale = new Vector3(facing/transform.parent.localScale.x,1,1);
    }
}
