Welcome to the Edge of Everything! (Press Return to continue)
This game is meant to allow you to better understand general relativity.
You will be able to experience and play with an approximation of space-time distorsion.
This is due to the fact that in this world, the speed of light is much smaller than in our own!
Or your character is moving insanely fast, that remains to be determined...
Try it a bit, then jump into the void when ready.

As you can see, space distorsion has been turned off for now.
You may attempt to proceed if you like, but you wont make it.

Luckily for you, space-time is on your side!
By distorting space, the gap that you were unable to cross will become trivial.

Now go forth my wayward son!
Master the space time continuum and make me proud!

Great job thus far!
From now on, you will experience the thrill of time dilation!
Gotta go fast boi.

This room is different from the others.
You need to catch the red, green and orange flags before the yellow one.
Good luck, you are nearly there!

Gongratulations!
You've beaten the game.
Of course, time and space dilations dont work like this in our reality. For once, the y and z axis are actually affected too.
But we hope your intuition about space-time improved, if only just a bit, by playing our game.
